const express = require("express");
const db = require("../db");
const utils = require("../utils");

const router = express.Router();

router.get("/", (req, res) => {
  const query = `select id, firstName,lastName,email,password from person`;
  db.pool.execute(query, (error, persons) => {
    res.send(utils.createResult(error, persons));
  });
});

module.exports = router;
