create table person (id integer primary key auto_increment, firstName varchar(20), lastName varchar(20), email varchar(30), password varchar(20));
insert into person (firstName,lastName,email,password) values('user1','user1','user1@test.com','1234');
insert into person (firstName,lastName,email,password) values('user2','user2','user2@test.com','4567');
insert into person (firstName,lastName,email,password) values('user3','user3','user3@test.com','123');